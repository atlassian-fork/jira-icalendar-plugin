package com.atlassian.jira.extra.icalfeed.util;

import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.extra.icalfeed.service.EntityAsEventService;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.jql.builder.JqlClauseBuilder;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.jql.context.QueryContext;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.query.Query;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Collections2;
import org.apache.commons.lang.StringUtils;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class QueryUtil
{
    private static final String DATE_FIELD_VERSIONDUE = "versiondue";

    private static final String CUSTOMFIELD_PREFIX = "customfield_";

    private final SearchService searchService;

    private final ProjectManager projectManager;

    private final PermissionManager permissionManager;

    private final CustomFieldManager customFieldManager;

    public QueryUtil(SearchService searchService, ProjectManager projectManager, PermissionManager permissionManager, CustomFieldManager customFieldManager)
    {
        this.searchService = searchService;
        this.projectManager = projectManager;
        this.permissionManager = permissionManager;
        this.customFieldManager = customFieldManager;
    }

    public Set<Project> getBrowseableProjectsFromQuery(final ApplicationUser user, Query searchQuery)
    {
        QueryContext simpleQueryContext = searchService.getSimpleQueryContext(user, searchQuery);
        Collection queryProjects = simpleQueryContext.getProjectIssueTypeContexts();
        Set<Project> projectsInQuery = new HashSet<Project>();

        for (Object contextObject : queryProjects)
        {
            projectsInQuery.addAll(
                    Collections2.filter(
                            Collections2.transform(
                                    ((QueryContext.ProjectIssueTypeContexts) contextObject).getProjectIdInList(),
                                    new Function<Long, Project>()
                                    {
                                        public Project apply(Long projectId)
                                        {
                                            return projectManager.getProjectObj(projectId);
                                        }
                                    }
                            ),
                            Predicates.and(Predicates.<Project>notNull(), new Predicate<Project>()
                            {
                                public boolean apply(Project aProject)
                                {
                                    return permissionManager.hasPermission(Permissions.BROWSE, aProject, user);
                                }
                            })
                    )
            );
        }

        return projectsInQuery;
    }

    public Query getQueryWithDateFieldsOptimised(Query query, Collection<String> singleDateFields, Collection<EntityAsEventService.Duration> durations, long start, long end)
    {
        Set<String> allDateFields = new HashSet<String>();
        if (!(null == singleDateFields || singleDateFields.isEmpty()))
        {
            for (String singleDateField : singleDateFields)
            {
                allDateFields.add(singleDateField);
            }
        }

        JqlQueryBuilder jqlQueryBuilder = JqlQueryBuilder.newBuilder(query);
        JqlClauseBuilder jqlClauseBuilder = jqlQueryBuilder.where().and().sub();
        int nFieldsProcessed = 0;

        if (!allDateFields.isEmpty())
        {
            for (String dateField : allDateFields)
            {
                if (processField(jqlClauseBuilder, dateField, nFieldsProcessed > 0, start, end))
                {
                    ++nFieldsProcessed;
                }
            }
        }

        if (!(null == durations || durations.isEmpty()))
        {
            for (EntityAsEventService.Duration duration : durations)
            {
                if (processField(jqlClauseBuilder, duration.getStartDateFieldKey(), nFieldsProcessed > 0, start, end))
                {
                    ++nFieldsProcessed;
                }

                if (processEndDateField(jqlClauseBuilder, duration.getEndDateFieldKey(), nFieldsProcessed > 0, start))
                {
                    ++nFieldsProcessed;
                }
            }
        }

        if (nFieldsProcessed > 0)
        {
            jqlClauseBuilder.endsub();
            return jqlQueryBuilder.buildQuery();
        }

        return query;
    }

    private boolean processField(JqlClauseBuilder jqlClauseBuilder, String dateField, boolean prependOr, long start, long end)
    {
        boolean fieldProcessed = false;

        if (StringUtils.startsWith(dateField, CUSTOMFIELD_PREFIX))
        {
            long customFieldId = getCustomFieldIdLong(dateField);
            if (-1 != customFieldId)
            {
                if (prependOr)
                    jqlClauseBuilder = jqlClauseBuilder.or();

                jqlClauseBuilder.customField(customFieldId).isNotEmpty();
                if(start > 0 && end > 0)
                {
                    jqlClauseBuilder.and().customField(customFieldId).gtEq(start).and().customField(customFieldId).ltEq(end);
                }
                fieldProcessed = true;
            }
        }
        else if (!StringUtils.equals(DATE_FIELD_VERSIONDUE, dateField))
        {
            if (prependOr)
                jqlClauseBuilder = jqlClauseBuilder.or();

            jqlClauseBuilder.field(dateField).isNotEmpty();
            if(start > 0 && end > 0)
            {
                jqlClauseBuilder.and().field(dateField).gtEq(start).and().field(dateField).ltEq(end);
            }
            fieldProcessed = true;
        }

        return fieldProcessed;
    }

    private boolean processEndDateField(JqlClauseBuilder jqlClauseBuilder, String endDateField, boolean prependOr, long start)
    {
        boolean fieldProcessed = false;

        if (StringUtils.startsWith(endDateField, CUSTOMFIELD_PREFIX))
        {
            long customFieldId = getCustomFieldIdLong(endDateField);
            if (-1 != customFieldId)
            {
                if (prependOr)
                {
                    jqlClauseBuilder = jqlClauseBuilder.or();
                }

                jqlClauseBuilder.customField(customFieldId).isNotEmpty();
                if(start > 0)
                {
                    jqlClauseBuilder.and().customField(customFieldId).gt(start);
                }
                fieldProcessed = true;
            }
        }
        else if (!StringUtils.equals(DATE_FIELD_VERSIONDUE, endDateField))
        {
            if (prependOr)
            {
                jqlClauseBuilder = jqlClauseBuilder.or();
            }

            jqlClauseBuilder.field(endDateField).isNotEmpty();
            if(start > 0)
            {
                jqlClauseBuilder.and().field(endDateField).gt(start);
            }
            fieldProcessed = true;
        }

        return fieldProcessed;
    }

    private long getCustomFieldIdLong(String customFieldId)
    {
        CustomField customField = customFieldManager.getCustomFieldObject(customFieldId);
        return null == customField ? -1 : customField.getIdAsLong();
    }
}
