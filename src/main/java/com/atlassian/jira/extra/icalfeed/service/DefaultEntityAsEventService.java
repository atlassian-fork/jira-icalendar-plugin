package com.atlassian.jira.extra.icalfeed.service;

import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.extra.icalfeed.dateprovider.DateProvider;
import com.atlassian.jira.extra.icalfeed.dateprovider.DateProviderModuleDescriptor;
import com.atlassian.jira.extra.icalfeed.util.QueryUtil;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.resolution.Resolution;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.query.Query;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Collections2;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DefaultEntityAsEventService implements EntityAsEventService
{
    private static final Logger LOG = LoggerFactory.getLogger(DefaultEntityAsEventService.class);

    private final JiraAuthenticationContext jiraAuthenticationContext;

    private final FieldManager fieldManager;

    private final Collection<DateProvider> builtInProviders;

    private final DateProvider customFieldDateProvider;

    private final PluginAccessor pluginAccessor;

    private final SearchService searchService;

    private final QueryUtil queryUtil;

    private final ApplicationProperties applicationProperties;

    public DefaultEntityAsEventService(JiraAuthenticationContext jiraAuthenticationContext, FieldManager fieldManager, Collection<DateProvider> builtInProviders , DateProvider customFieldDateProvider, PluginAccessor pluginAccessor, SearchService searchService, QueryUtil queryUtil, ApplicationProperties applicationProperties)
    {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.fieldManager = fieldManager;
        this.builtInProviders = builtInProviders;
        this.customFieldDateProvider = customFieldDateProvider;
        this.pluginAccessor = pluginAccessor;
        this.searchService = searchService;
        this.queryUtil = queryUtil;
        this.applicationProperties = applicationProperties;
    }

    @Override
    public Result search(Query query, Collection<String> dateFieldNames, Collection<Duration> durations, boolean includeFixVersions, ApplicationUser user, long start, long end, int maxIssue) throws SearchException, ParseException
    {
        Map<Field, DateProvider> fieldToDateProviders = getFieldToDateProviderMap(dateFieldNames);
        Map<Duration, DurationDateProviders> durationToDateProviders = getDurationToDateProviders(durations);

        Query originalQuery = query;
        query = queryUtil.getQueryWithDateFieldsOptimised(query, dateFieldNames, durations, start, end);
        if (LOG.isDebugEnabled())
            LOG.debug(String.format("Original query: %s\nRewritten query: %s", originalQuery, query));

        int maxFilter = maxIssue > 0 ? maxIssue : Integer.parseInt(applicationProperties.getDefaultBackedString(APKeys.JIRA_SEARCH_VIEWS_DEFAULT_MAX));

        Collection<Issue> issueResults = searchService.search(
                user,
                query,
                new PagerFilter(maxFilter)
        ).getResults();

        Collection<IssueDateResult> issueDateResults = new ArrayList<IssueDateResult>();
        I18nHelper i18nHelper = jiraAuthenticationContext.getI18nHelper();

        for (Issue issue : issueResults)
        {
            populateSingleDatesForIssue(issue, fieldToDateProviders, i18nHelper, issueDateResults);
            populateDurationsForIssue(issue, durationToDateProviders, i18nHelper, issueDateResults);
        }

        return new Result(
                issueDateResults,
                Collections.<Version>emptySet(),
                includeFixVersions ? getVersionsFromQuery(query, user) : Collections.<Version> emptySet()
        );
    }

    private void populateDurationsForIssue(Issue issue, Map<Duration, DurationDateProviders> durationToDateProviders, I18nHelper i18nHelper, Collection<IssueDateResult> issueDateResults)
    {
        for (Map.Entry<Duration, DurationDateProviders> durationToDateProvidersEntry : durationToDateProviders.entrySet())
        {
            DurationDateProviders durationDatesProvider = durationToDateProvidersEntry.getValue();
            DateProvider startDateProvider = durationDatesProvider.getStartDateProvider();
            DateProvider endDateProvider = durationDatesProvider.getEndDateProvider();

            if (null != startDateProvider && null != endDateProvider)
            {
                Field startField = durationDatesProvider.getStartDateField();
                Field endField = durationDatesProvider.getEndDateField();

                LOG.debug(String.format(
                        "Start date provider (%s) : %s; End date provider (%s) : %s",
                        startField,
                        startDateProvider.getClass().getName(),
                        endField,
                        endDateProvider.getClass().getName()
                ));

                DateTime startDate = startDateProvider.getStart(issue, startField);
                DateTime endDate = endDateProvider.getStart(issue, endField);

                if (startField.getId().equals(IssueFieldConstants.CREATED) && endField.getId().equals(IssueFieldConstants.DUE_DATE))
                {
                    endDate = reinterpretEndDate(startDate, endDate);
                }

                LOG.debug(String.format("Duration fields: %s = %s; %s = %s.", startField.getId(), startDate, endField.getId(), endDate));

                if (!(null == startDate || null == endDate || startDate.isAfter(endDate)))
                {
                    boolean allDay = startDateProvider.isAllDay(issue, startField) && endDateProvider.isAllDay(issue, endField);
                    if (allDay)
                        endDate = endDate.plusDays(1); // JIRA all day dates are inclusive (I think), so we have to adjust for the end date (for us, the end date is exclusive).

                    IssueType issueType = issue.getIssueTypeObject();
                    Status issueStatus = issue.getStatusObject();

                    issueDateResults.add(
                            new DurationIssueResult(
                                    issue.getAssignee(),
                                    allDay,
                                    issue.getKey(),
                                    issue.getSummary(),
                                    issue.getDescription(),
                                    issueType.getNameTranslation(i18nHelper),
                                    issueType.getIconUrl(),
                                    issueStatus.getNameTranslation(i18nHelper),
                                    getResolution(issue),
                                    issueStatus.getIconUrl(),
                                    new DateTime(issue.getCreated().getTime()),
                                    new DateTime(issue.getUpdated().getTime()),
                                    startDate,
                                    endDate,
                                    startField.getId(),
                                    startField.getName(),
                                    endField.getId(),
                                    endField.getName()
                            )
                    );
                }
            }
        }
    }

    private String getResolution(Issue issue)
    {
        final Resolution resolutionObject = issue.getResolutionObject();
        return resolutionObject == null ? null : resolutionObject.getName();
    }

    private void populateSingleDatesForIssue(Issue issue, Map<Field, DateProvider> fieldToDateProviders, I18nHelper i18nHelper, Collection<IssueDateResult> issueDateResults)
    {
        for (Map.Entry<Field, DateProvider> singleDateFieldProviderEntry : fieldToDateProviders.entrySet())
        {
            DateProvider singleDateFieldProvider = singleDateFieldProviderEntry.getValue();
            if (null != singleDateFieldProvider)
            {
                Field dateField = singleDateFieldProviderEntry.getKey();
                DateTime start = singleDateFieldProvider.getStart(issue, dateField);
                DateTime end = singleDateFieldProvider.getEnd(issue, dateField, start);

                if (null != start && null != end)
                {
                    IssueType issueType = issue.getIssueTypeObject();
                    Status issueStatus = issue.getStatusObject();

                    issueDateResults.add(
                            new SingleDateIssueResult(
                                    issue.getAssignee(),
                                    singleDateFieldProvider.isAllDay(issue, dateField),
                                    issue.getKey(),
                                    issue.getSummary(),
                                    issue.getDescription(),
                                    issueType.getNameTranslation(i18nHelper),
                                    issueType.getIconUrl(),
                                    issueStatus.getNameTranslation(i18nHelper),
                                    getResolution(issue),
                                    issueStatus.getIconUrl(),
                                    new DateTime(issue.getCreated().getTime()),
                                    new DateTime(issue.getUpdated().getTime()),
                                    start,
                                    end,
                                    dateField.getId(),
                                    dateField.getName()
                            )
                    );
                }
            }
        }
    }

    private Map<Duration, DurationDateProviders> getDurationToDateProviders(Collection<Duration> durations)
    {
        Map<Duration, DurationDateProviders> durationToDateProviders = new HashMap<Duration, DurationDateProviders>();

        if (null != durations)
            for (Duration duration : durations)
            {
                Field startDateField = fieldManager.getField(StringUtils.defaultString(duration.getStartDateFieldKey()));
                DateProvider startDateProvider = getFirstDateProviderForField(startDateField);

                Field endDateField = fieldManager.getField(StringUtils.defaultString(duration.getEndDateFieldKey()));
                DateProvider endDateProvider = getFirstDateProviderForField(endDateField);

                durationToDateProviders.put(
                        duration,
                        new DurationDateProviders(
                                startDateField,
                                startDateProvider,
                                endDateField,
                                endDateProvider
                        )
                );
            }

        return durationToDateProviders;
    }

    private Map<Field, DateProvider> getFieldToDateProviderMap(Collection<String> dateFieldNames)
    {
        Map<Field, DateProvider> fieldToDateProviders = new HashMap<Field, DateProvider>();

        if (null != dateFieldNames)
        {
            Collection<Field> validFields = Collections2.filter(
                    Collections2.transform(
                            dateFieldNames,
                            new Function<String, Field>()
                            {
                                @Override
                                public Field apply(String fieldName)
                                {
                                    return fieldManager.getField(StringUtils.defaultString(fieldName));
                                }
                            }
                    ),
                    Predicates.notNull()
            );

            for (Field validField : validFields)
                fieldToDateProviders.put(
                        validField,
                        getFirstDateProviderForField(validField)
                );
        }

        return fieldToDateProviders;
    }

    private DateProvider getFirstDateProviderForField(Field dateField)
    {
        if (null != dateField)
        {
            Collection<DateProvider> dateProvidersForField = getDateProvider(dateField);
            if (!(null == dateProvidersForField || dateProvidersForField.isEmpty()))
                return dateProvidersForField.iterator().next();
        }

        return null;
    }

    private Collection<Version> getVersionsFromQuery(Query query, ApplicationUser user)
    {
        Collection<Version> versions = new ArrayList<Version>();

        for (Project project : queryUtil.getBrowseableProjectsFromQuery(user, query))
            versions.addAll(Collections2.filter(
                    project.getVersions(),
                    Predicates.and(
                            Predicates.<Object>notNull(),
                            new Predicate<Version>()
                            {
                                @Override
                                public boolean apply(Version version)
                                {
                                    return null != version.getReleaseDate();
                                }
                            }
                    )
            ));

        return versions;
    }

    /* Wider visibility scope for tests */
    Collection<DateProvider> getDateProvider(final Field field)
    {
        List<DateProvider> dateProviders = new ArrayList<DateProvider>();

        dateProviders.addAll(
                Collections2.filter(
                        builtInProviders,
                        new Predicate<DateProvider>()
                        {
                            @Override
                            public boolean apply(DateProvider builtInDateProvider)
                            {
                                return builtInDateProvider.handles(field);
                            }
                        }
                )
        );

        if (field instanceof CustomField)
        {
            List<DateProvider> customFieldDateOverrideProviders = new ArrayList<DateProvider>();
            List<DateProviderModuleDescriptor> dictionaryModuleDescriptors = pluginAccessor.getEnabledModuleDescriptorsByClass(DateProviderModuleDescriptor.class);
            if (null != dictionaryModuleDescriptors)
                for (DateProviderModuleDescriptor dateProviderModuleDescriptor : dictionaryModuleDescriptors)
                {
                    DateProvider dateProvider = dateProviderModuleDescriptor.getModule();
                    if (dateProvider.handles(field))
                        customFieldDateOverrideProviders.add(dateProvider);
                }

            if (customFieldDateOverrideProviders.isEmpty() && customFieldDateProvider.handles(field))
            {
                dateProviders.add(customFieldDateProvider);
            }
            else
            {
                dateProviders.addAll(customFieldDateOverrideProviders);
            }
        }

        return dateProviders;
    }

    private static class DurationDateProviders
    {
        private final Field startDateField;

        private final DateProvider startDateProvider;

        private final Field endDateField;

        private final DateProvider endDateProvider;

        private DurationDateProviders(Field startDateField, DateProvider startDateProvider, Field endDateField, DateProvider endDateProvider)
        {
            this.startDateField = startDateField;
            this.startDateProvider = startDateProvider;
            this.endDateField = endDateField;
            this.endDateProvider = endDateProvider;
        }

        public Field getStartDateField()
        {
            return startDateField;
        }

        public DateProvider getStartDateProvider()
        {
            return startDateProvider;
        }

        public Field getEndDateField()
        {
            return endDateField;
        }

        public DateProvider getEndDateProvider()
        {
            return endDateProvider;
        }
    }

    private DateTime reinterpretEndDate(DateTime start, DateTime end)
    {
        if (isSameDayButBefore(start, end))
            return end.withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).withMillisOfSecond(999);

        return end;
    }

    private boolean isSameDayButBefore(DateTime start, DateTime end)
    {
        return start != null
                && end != null
                && start.getYear() == end.getYear()
                && start.getMonthOfYear() == end.getMonthOfYear()
                && start.getDayOfMonth() == end.getDayOfMonth()
                && start.isAfter(end);
    }
}
