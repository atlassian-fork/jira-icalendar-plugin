package com.atlassian.jira.extra.icalfeed.dateprovider;


import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CreatedSystemField;
import com.atlassian.jira.issue.fields.Field;

import java.util.Date;

import org.joda.time.DateTime;

public class CreatedDateProvider extends AbstractSystemDateProvider
{
    @Override
    protected Date getStartDateInternal(Issue issue, Field field)
    {
        return issue.getCreated();
    }

    @Override
    public boolean handles(Field field)
    {
        return field instanceof CreatedSystemField;
    }

    @Override
    public boolean isAllDay(Issue issue, Field field)
    {
        return false;
    }

    @Override
    public DateTime getEnd(Issue issue, Field field, DateTime startDate)
    {
        return null == startDate ? null : startDate.plusMinutes(60 * (isAllDay(issue, field) ? 24 : 0));
    }
}
