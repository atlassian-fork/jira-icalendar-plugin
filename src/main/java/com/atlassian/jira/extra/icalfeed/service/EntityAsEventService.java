package com.atlassian.jira.extra.icalfeed.service;

import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.query.Query;
import org.joda.time.DateTime;

import java.text.ParseException;
import java.util.Collection;

public interface EntityAsEventService
{
    Result search(Query query, Collection<String> dateFieldNames, Collection<Duration> durations, boolean includeFixVersions, ApplicationUser user, long start, long end, int maxIssue) throws SearchException, ParseException;

    static class Duration
    {
        private String startDateFieldKey;

        private String endDateFieldKey;

        public Duration(String startDateFieldKey, String endDateFieldKey)
        {
            this.startDateFieldKey = startDateFieldKey;
            this.endDateFieldKey = endDateFieldKey;
        }

        public String getStartDateFieldKey()
        {
            return startDateFieldKey;
        }

        public String getEndDateFieldKey()
        {
            return endDateFieldKey;
        }

        @Override
        public boolean equals(Object o)
        {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Duration duration = (Duration) o;

            if (endDateFieldKey != null ? !endDateFieldKey.equals(duration.endDateFieldKey) : duration.endDateFieldKey != null)
                return false;
            if (startDateFieldKey != null ? !startDateFieldKey.equals(duration.startDateFieldKey) : duration.startDateFieldKey != null)
                return false;

            return true;
        }

        @Override
        public int hashCode()
        {
            int result = startDateFieldKey != null ? startDateFieldKey.hashCode() : 0;
            result = 31 * result + (endDateFieldKey != null ? endDateFieldKey.hashCode() : 0);
            return result;
        }
    }

    static class Result
    {
        public final Collection<IssueDateResult> issues;

        public final Collection<Version> affectedVersions;

        public final Collection<Version> fixedVersions;

        public Result(Collection<IssueDateResult> issues, Collection<Version> affectedVersions, Collection<Version> fixedVersions)
        {
            this.issues = issues;
            this.affectedVersions = affectedVersions;
            this.fixedVersions = fixedVersions;
        }
    }

    static abstract class IssueDateResult
    {
        public final ApplicationUser assignee;

        public final boolean allDay;

        public final String issueKey;

        public final String issueSummary;

        public final String issueDescription;

        public final String type;

        public final String typeIconUrl;

        public final String status;

        public final String resolution;

        public final String statusIconUrl;

        public final DateTime issueCreated;

        public final DateTime issueUpdated;

        public final DateTime start;

        public final DateTime end;

        public IssueDateResult(ApplicationUser assignee, boolean allDay, String issueKey, String issueSummary, String issueDescription, String type, String typeIconUrl, String status, String resolution, String statusIconUrl, DateTime issueCreated, DateTime issueUpdated, DateTime start, DateTime end)
        {
            this.assignee = assignee;
            this.allDay = allDay;
            this.issueKey = issueKey;
            this.issueSummary = issueSummary;
            this.issueDescription = issueDescription;
            this.type = type;
            this.typeIconUrl = typeIconUrl;
            this.status = status;
            this.resolution = resolution;
            this.statusIconUrl = statusIconUrl;
            this.issueCreated = issueCreated;
            this.issueUpdated = issueUpdated;
            this.start = start;
            this.end = end;
        }
    }

    static class SingleDateIssueResult extends IssueDateResult
    {
        public final String dateFieldKey;

        public final String dateFieldName;

        public SingleDateIssueResult(ApplicationUser assignee, boolean allDay, String issueKey, String issueSummary, String issueDescription, String type, String typeIconUrl, String status, String rawStatus, String statusIconUrl, DateTime issueCreated, DateTime issueUpdated, DateTime start, DateTime end, String dateFieldKey, String dateFieldName)
        {
            super(assignee, allDay, issueKey, issueSummary, issueDescription, type, typeIconUrl, status, rawStatus, statusIconUrl, issueCreated, issueUpdated, start, end);
            this.dateFieldKey = dateFieldKey;
            this.dateFieldName = dateFieldName;
        }
    }

    static class DurationIssueResult extends IssueDateResult
    {
        public final String startDateFieldKey;

        public final String startDateFieldName;

        public final String endDateFieldKey;

        public final String endDateFieldName;

        public DurationIssueResult(ApplicationUser assignee, boolean allDay, String issueKey, String issueSummary, String issueDescription, String type, String typeIconUrl, String status, String resolution, String statusIconUrl, DateTime issueCreated, DateTime issueUpdated, DateTime start, DateTime end, String startDateFieldKey, String startDateFieldName, String endDateFieldKey, String endDateFieldName)
        {
            super(assignee, allDay, issueKey, issueSummary, issueDescription, type, typeIconUrl, status, resolution, statusIconUrl, issueCreated, issueUpdated, start, end);
            this.startDateFieldKey = startDateFieldKey;
            this.startDateFieldName = startDateFieldName;
            this.endDateFieldKey = endDateFieldKey;
            this.endDateFieldName = endDateFieldName;
        }
    }
}
