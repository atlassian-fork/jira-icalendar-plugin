package com.atlassian.jira.extra.icalfeed.dateprovider;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.ResolutionDateSystemField;

import java.util.Date;

import org.joda.time.DateTime;

public class ResolutionDateProvider extends AbstractSystemDateProvider
{
    @Override
    protected Date getStartDateInternal(Issue issue, Field field)
    {
        return issue.getResolutionDate();
    }

    @Override
    public boolean handles(Field field)
    {
        return field instanceof ResolutionDateSystemField;
    }

    @Override
    public boolean isAllDay(Issue issue, Field field)
    {
        return false;
    }

    @Override
    public DateTime getEnd(Issue issue, Field field, DateTime startDate)
    {
        return null == startDate ? null : startDate.plusMinutes(60 * (isAllDay(issue, field) ? 24 : 0));
    }
}
