package com.atlassian.jira.extra.icalfeed.dateprovider;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.UpdatedSystemField;

import java.util.Date;

import org.joda.time.DateTime;

public class UpdatedDateProvider extends AbstractSystemDateProvider
{
    @Override
    protected Date getStartDateInternal(Issue issue, Field field)
    {
        return issue.getUpdated();
    }

    @Override
    public boolean handles(Field field)
    {
        return field instanceof UpdatedSystemField;
    }

    @Override
    public boolean isAllDay(Issue issue, Field field)
    {
        return false;
    }

    @Override
    public DateTime getEnd(Issue issue, Field field, DateTime startDate)
    {
        return null == startDate ? null : startDate.plusMinutes(60 * (isAllDay(issue, field) ? 24 : 0));
    }
}
