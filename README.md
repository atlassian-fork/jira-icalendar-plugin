# Jira iCalendar plugin 

## Description

This (Jira bundled) plugin provides REST API for getting Jira events in iCal format.


## Branches
- master - Jira 8.0+
- jira-icalendar-plugin-1.4.x - Jira 7.6.x, Jira 7.13.x (in general: pre-8.0)